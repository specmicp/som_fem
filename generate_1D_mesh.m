% ===========================
%
% title: generate_1D_mesh
% date: 12/01/2019
% author: Fabien Georget
%
% ===========================
%
% Description: Generate a 1D mesh
%
% parameters:
% - total_length: length of the system
% - number_of_nodes: number of nodes in the mesh
% - cross_section: cross section of the system

function mesh = generate_1D_mesh(total_length, number_of_nodes, cross_section)

mesh.nsd=1; % Number of space dimensions
mesh.nen=2; % Number of nodes per element

mesh.nnp = number_of_nodes; % Total number of nodes
mesh.nel = mesh.nnp-1; % Total number of elements

mesh.A = cross_section; % The cross section

dx = total_length/mesh.nel; % The size of an element

mesh.xn = zeros(mesh.nnp, mesh.nsd); % The coordinates of the element
for N=1:mesh.nnp
  mesh.xn(N,1)=(N-1)*dx; % Fill the mesh
end;
mesh.xn(N,1)=total_length; % Because of floating point operations

% The connectivity matrix
%
% For each element: [index of 1st node; index of 2nd node]
mesh.ien = zeros(mesh.nen,mesh.nel);
for e=1:mesh.nel
  mesh.ien(1,e)=e;
  mesh.ien(2,e)=e+1;
end

