% ===========================
%
% title: generate_1D_boundary_conditions
% date: 12/01/2019
% author: Fabien Georget
%
% ===========================
%
% Description: Creates the structure containing the boundary conditions
%
% Parameters:
% - mesh: the mesh
% - fixed_boundary_conditions: cell containing 2-tuples [node, value]
% 
% Return parameters:
% - idb: 1 if essential boundary conditions at that node, 0 otherwise
% - g: vector containing the fixed values
% - f: vector containing the fixed fluxes

function [idb, g, f] = generate_1D_boundary_conditions(mesh, fixed_boundary_conditions)
  
  ndf = 1;
  
  idb = zeros(mesh.nnp,ndf);
  g = zeros(mesh.nnp,ndf);
  f = zeros(mesh.nnp,ndf);
  
  nb_bcs=size(fixed_boundary_conditions);
  for bc=1:nb_bcs(1)
    idb(fixed_boundary_conditions{bc}(1), 1) = 1;
    g(fixed_boundary_conditions{bc}(1)) = fixed_boundary_conditions{bc}(2);
  end