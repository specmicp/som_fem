% ===========================
%
% title: compute_1D_diffusion_stiffness_matrix
% date: 12/01/2019
% author: Fabien Georget
%
% ===========================
%
% Description: Compute the element stiffness matrix for the 1D diffusion equation
%
% Parameters:
% - e: the number of the element
% - mesh: the mesh
% - material: the material parameters
%
% Return parameters:
% - ke:  The element stiffness matrix for the 1D diffusion equation
%

function ke = compute_1D_diffusion_stiffness_matrix(e, mesh, material)
  l = mesh.xn(mesh.ien(2,e),1)-mesh.xn(mesh.ien(1,e),1);
  ke = material.D * mesh.A / l * [[1 -1]; [-1 1]];