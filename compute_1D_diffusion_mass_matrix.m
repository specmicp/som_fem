% ===========================
%
% title: compute_1D_diffusion_mass_matrix
% date: 12/01/2019
% author: Fabien Georget
%
% ===========================
%
% Description: Compute the element mass matrix for the 1D diffusion equation
%
% Parameters:
% - e: the number of the element
% - mesh: the mesh
% - material: the material parameters
%
% Return parameters:
% - me:  The element mass matrix for the 1D diffusion equation
%

function me = compute_1D_diffusion_mass_matrix(e, mesh, material)
  l = mesh.xn(mesh.ien(2,e),1)-mesh.xn(mesh.ien(1,e),1);
  me = material.phi * mesh.A * l / 2 * [[1 0]; [0 1]];