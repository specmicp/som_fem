% ===========================
%
% title: compute_linear_system
% date: 12/01/2019
% author: Fabien Georget
%
% ===========================
%
% Description: Return the linear to solve for the 1D diffusion equation
%
% Parameters:
% - dnp: the current displacement
% - vnp: the current velocity
% - mesh: the mesh
% - eq: the number of equations
% - material: the material parameters
% - alpha: the time integration parameters
% - timestep: the duration of a timestep
%
% Return parameters:
% - r: the right-hand side vector
% - A: the left-hand side matrix

function [r, A] = compute_linear_system(dnp, vnp, mesh, eq, material, alpha, timestep)
  
  r = zeros(eq.neq,1);
  A = zeros(eq.neq, eq.neq);
  
  for e=1:mesh.nel
    N1 = mesh.ien(1,e);
    N2 = mesh.ien(2,e);
    
    % Element matrices
    me = compute_1D_diffusion_mass_matrix(e, mesh, material);
    ke = compute_1D_diffusion_stiffness_matrix(e, mesh, material);
    
    % Element residual
    de = [dnp(N1); dnp(N2)];
    ve = [vnp(N1); vnp(N2)];
    re = -me*ve -ke*de;
    
    % Element LHS
    ae = me + alpha*timestep*ke;    

    % Assembly
    for nr=1:mesh.nen
        if (eq.id(mesh.ien(nr,e),1)>0)
          P = eq.id(mesh.ien(nr,e),1);
          % RHS assembly
          r(P) = r(P) + re(nr);
          
          for nc=1:mesh.nen
            if (eq.id(mesh.ien(nc,e),1)>0)
              Q = eq.id(mesh.ien(nc,e),1);
              % LHS assembly
              A(P,Q) = A(P,Q) + ae(nr,nc);
            end
          end
          
        end
    end
    
  end