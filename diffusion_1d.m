% ===========================
%
% title: diffusion_1d
% date: 12/01/2019
% author: Fabien Georget
%
% Inspiration: code from J.H. Prevost and David Luet (Princeton University)
%
% ===========================
%
% Description: Solve the 1D diffusion equation
%

% Material properties
% ===================
material.D = 1e-2; % diffusion coefficient
material.phi = 0.3; %

cross_section = 1;% Area
total_length = 10;%
number_of_nodes = 51;%
fixed_boundary_conditions = {[1, 10]};%

ndf = 1; % number of degree of freedom
         % for diffusion: =1, the concentration

% numerical properties
% ====================
timestep = 0.5;%
alpha = 1; %


% Mesh 
% ====
mesh = generate_1D_mesh(total_length, number_of_nodes, cross_section);

% Boundary Conditions
% ===================
[idb, g, f] = generate_1D_boundary_conditions(mesh, fixed_boundary_conditions);

% assumption
g_dot = zeros(mesh.nnp,1); 

% initialize
% ==========
d = g;
v = g_dot;

% number the equations
% --------------------
eq = number_equations(mesh, idb, ndf);

% Run the algorithm
% =================
for n=1:100
  [d, v] = one_timestep(d, v, mesh, eq, material, alpha, timestep);
end


plot(mesh.xn, d, "+")
