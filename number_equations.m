% ===========================
%
% title: number_equations
% date: 12/01/2019
% author: Fabien Georget
%
% ===========================
%
% Description: Solve the 1D diffusion equation
%
% Parameters:
% - mesh: the mesh
% - idb: the boundary conditions id (see generate_1D_boundary_conditions)
% - ndf: the number of degree of freedom

% Return parameters:
% - eq: the equation numbering

function eq = number_equations(mesh, idb,ndf)


eq.id=zeros(mesh.nnp,ndf);  % The id of the equations
eq.neq=0;                        % The total number of equations

for N=1:mesh.nnp
    for i=1:ndf
        if (idb(N,i) == 0)  
            eq.neq=eq.neq+1;
            eq.id(N,i)=eq.neq;
        end;
    end;
end;     