% ===========================
%
% title: one_timestep
% date: 12/01/2019
% author: Fabien Georget
%
% ===========================
%
% Description: Solve one timestep of the 1D diffusion equation
%
% Parameters:
% - d: the initial displacement
% - v: the initial velocity
% - mesh: the mesh
% - eq: the equation numbers
% - material: the material properties
% - alpha: alpha: the time integration parameters
% - timestep: the time discretisation
%
% Return parameters:
% - dnp: the displacement at the end of the timestep
% - vnp: the velocity at the end of the timestep

function [dnp, vnp] = one_timestep(d, v, mesh, eq, material, alpha, timestep)
  
  predictor = d + (1-alpha)*timestep*v;
  vnp = zeros(mesh.nnp,1); %g_dot if it exists !
  dnp = predictor + alpha*timestep*vnp;

  [r, A] = compute_linear_system(dnp, vnp, mesh, eq, material, alpha, timestep);
  vnp_sol = A\r;
  
  %assemble vnp
  for N=1:mesh.nnp
    if (eq.id(N,1)>0)
      P = eq.id(N,1);
      vnp(N) = vnp_sol(P);
    end
  end
  dnp = predictor + alpha*timestep*vnp;
